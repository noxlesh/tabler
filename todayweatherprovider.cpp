#include "todayweatherprovider.h"


TodayWeatherProvider::WeatherStatus TodayWeatherProvider::parseWeatherStatus(QString rawStr)
{
    QRegularExpression weatherStatusRx("weatherIco d(\\d\\d\\d)");
    QRegularExpressionMatchIterator i = weatherStatusRx.globalMatch(rawStr);
    qDebug() << rawStr;
    if(i.hasNext()){
        uint wStatusCode = i.next().captured(1).toInt();
        qDebug() << "Weather status code: " << wStatusCode;
        return static_cast<WeatherStatus>(wStatusCode);
    }
    return WeatherStatus::Clear;

}

TodayWeatherProvider::TodayWeatherProvider(QQmlContext *ctx)
{
    ctx->setContextProperty("twp",this);
}

void TodayWeatherProvider::onConnFinished()
{
    if(rply->isReadable()) {

        QString rawData(rply->readAll());
        QRegularExpression todayOverallRx("<td class=\"p\\d[\\w\\s]+cur\\s?\"\\s?>(.+?)<\\/td>");
        QRegularExpressionMatchIterator i = todayOverallRx.globalMatch(rawData);
        qDebug() << i.next().captured(1); // Time range
        parseWeatherStatus(i.next().captured(1)); // Weather status
        QString temp(i.next().captured(1).replace("&deg;","\u00b0 C")); // Temperature
        qDebug() << i.next().captured(1); // Feel temperature
        qDebug() << i.next().captured(1); // Presshure
        qDebug() << i.next().captured(1); // Humidity
        qDebug() << i.next().captured(1); // Wind direction (needs to parse)
        qDebug() << i.next().captured(1); // Precipitation
        emit weatherUpdated(temp);
    } else {
        qDebug() << "Failed to read HTTP reply data";
    }
}

void TodayWeatherProvider::onConnError(QNetworkReply::NetworkError err)
{
    qDebug() << err;
}
