import QtQuick 2.7
import QtQuick.Layouts 1.1

Item {
    FontLoader {
        id: voxFont
        source: "qrc:/Vox.ttf"
    }
    ColumnLayout {
        anchors.fill: parent
        spacing: 0
        Item {
//        Rectangle {
//            color: "cyan"
            id: topBar
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: 35
            Layout.preferredHeight: 50
            Layout.maximumHeight: 50
            Layout.leftMargin: 10
            Layout.alignment: Qt.AlignTop
            Text {
                id: date
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: 10
                font.family: voxFont.name
                font.pointSize: 32
                color: "white"
                text: "Вc 21 Янв"
            }
        }
        Item {
        //Rectangle {
            //color: "blue"
            id: timeBar
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: 165
            Layout.preferredHeight: 230
            Layout.maximumHeight: 230
            Text {
                id: dots
                height: parent.height
                anchors.centerIn: parent
                font.family: voxFont.name
                minimumPointSize: 96
                font.pointSize: 240
                fontSizeMode: Text.Fit
                color: "white"
                text: ":"
                states: [
                    State {
                        name: "dotOn"
                        PropertyChanges {
                            target: dots
                            opacity: 0
                        }
                    },
                    State {
                        name: "dotOff"
                        PropertyChanges {
                            target: dots
                            opacity: 1
                        }
                    }
                ]
            }
            Text {
                height: parent.height
                anchors.right: dots.left
                anchors.rightMargin: 0
                horizontalAlignment: Text.AlignRight
                id: hh
                font.family: voxFont.name
                minimumPointSize: 96
                font.pointSize: 240
                fontSizeMode: Text.Fit
                color: "white"
                text: "00"
            }
            Text {
                height: parent.height
                anchors.left: dots.right
                //horizontalAlignment: Text.AlignLeft
                id: mm
                font.family: voxFont.name
                minimumPointSize: 96
                font.pointSize: 240
                fontSizeMode: Text.Fit
                color: "white"
                text: "00"
            }
        }
    }
    Timer {
        interval: 500
        running: true
        repeat: true
        triggeredOnStart: true
        onTriggered: {
            date.text = Qt.formatDateTime(new Date(),"ddd, dd MMM")
            hh.text = Qt.formatDateTime(new Date(),"hh")
            mm.text = Qt.formatDateTime(new Date(),"mm")
            if(dots.state == 'dotOff') {
                dots.state = "dotOn"
            } else {
                dots.state = "dotOff"
            }
        }
    }
}
