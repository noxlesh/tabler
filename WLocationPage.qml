import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

Item {


    ColumnLayout {
        anchors.fill: parent
        spacing: 0
        ToolBar {
            Layout.fillWidth: true
            RowLayout {
                anchors.fill: parent
                ToolButton {
                    text: qsTr("‹")
                    onClicked: {
                        winMain.wConfigured = false
                        stackSettings.pop()
                        wlspModel.clearData()
                    }
                }
                Label {
                    text: "Weather location"
                    elide: Label.ElideRight
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                    Layout.fillWidth: true
                }
            }
        }
        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: 380
            color: "#d0d4e0"
            TextField {
             z: 2
             id: textFieldWL
             width: parent.width
             placeholderText: "Search here"
             onTextChanged: wlsp.getHTTPContent("https://sinoptik.ua/search.php?q="+text);
            }
            ListView {
                anchors.top: textFieldWL.bottom
                anchors.bottom: parent.bottom
                width: parent.width
                id: listViewWL
                model: wlspModel
                delegate: ItemDelegate {
                    width: parent.width
                    text: location + " " + sublocation
                    onClicked: {      
                        winMain.wConfigured = true
                        winMain.wLocation = location
                        winMain.wSublocation = sublocation
                        winMain.wUrlParam = urlParam
                        setListModel.setProperty(0, "name", winMain.wLocation + " " + winMain.wSublocation)
                        console.log(setListModel.get(0).name)
                        stackSettings.pop()
                        wlspModel.clearData()
                    }
                }

            }

        }
    }
}
