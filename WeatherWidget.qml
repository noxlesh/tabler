import QtQuick 2.7
import QtQuick.Layouts 1.1

Rectangle {
    FontLoader {
        id: voxFont
        source: "qrc:/Vox.ttf"
    }
    color: "#303646"
    Connections {
        target: twp
        onWeatherUpdated: {
            temp_label.text = temp
        }
    }
    Text {
        id:temp_label
        //height: 0.8 * parent.height
        anchors.verticalCenter: parent.verticalCenter
        font.family: voxFont.name
        minimumPointSize: 32
        font.pointSize: 48
        fontSizeMode: Text.Fit
        color: "white"
    }
    Timer {
        interval: 3600000
        running: true
        repeat: true
        triggeredOnStart: true
        onTriggered: {
            twp.getHTTPContent("https://sinoptik.ua/"+winMain.wUrlParam)
        }
    }
}
