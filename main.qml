import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0

Window {
    //    property bool isPortrait: Screen.primaryOrientation === Qt.PortraitOrientation || Screen.primaryOrientation === Qt.InvertedPortraitOrientation
    //    onIsPortraitChanged: {
    //        var oMode = isPortrait ? "Portrait" : "Landscape"
    //        console.log("An orientation mode is", oMode)
    //        console.log("Width: ", width)
    //        console.log("Height: ", height)
    //    }
    visible: true
    width: 640
    height: 480
    minimumWidth: 640
    minimumHeight: 480
    //x: (Screen.width - width)/2
    //y: (Screen.height - height)/2
    title: qsTr("Tabler")
    color: "black"
    id: winMain
    signal btnClicked()

    property bool wConfigured: false
    property string wLocation: ""
    property string wSublocation: ""
    property string wUrlParam: ""
    Settings {
        id: ws
        property alias wConfigured: winMain.wConfigured
        property alias wLocation: winMain.wLocation
        property alias wSublocation: winMain.wSublocation
        property alias wUrlParam: winMain.wUrlParam
    }
    Component.onCompleted: {
        if(wConfigured)
            console.log(winMain.wConfigured)
            console.log(winMain.wLocation)
            console.log(winMain.wSublocation)
            console.log(winMain.wUrlParam)
    }
    StackView {
        id:stack
        anchors.fill: parent
        initialItem: mainPage
        Component {
            id:mainPage
            MainPage {
            }
        }
    }

}
