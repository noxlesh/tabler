#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H

#include <QObject>
#include <QQmlContext>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrl>
//#include "zlib.h"

class HTTPDataProvider : public QObject
{
    Q_OBJECT
public:
    explicit HTTPDataProvider(QObject *parent = nullptr);

public slots:
    void getHTTPContent(QString queryStr);
    virtual void onConnFinished() = 0;
    virtual void onConnError(QNetworkReply::NetworkError) = 0;

private:
    QNetworkAccessManager *qnam;

protected:
    QNetworkReply *rply;
//    QByteArray uncompress(const QByteArray &data);

};

#endif // HTTPCLIENT_H
