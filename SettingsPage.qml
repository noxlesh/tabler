import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

Item {
    ListModel {
        id: setListModel
        ListElement {
            name: "Please choose location"
            page: "qrc:/WLocationPage.qml"
        }
        ListElement {
            name: "item2"
            page: "qrc:/WLocationPage.qml"
        }
        ListElement {
            name: "item3"
            page: "qrc:/WLocationPage.qml"
        }
    }
    StackView {
        id:stackSettings
        anchors.fill: parent
        initialItem: Component {
            ColumnLayout {
                spacing: 0
                ToolBar {
                    Layout.fillWidth: true
                    RowLayout {
                        anchors.fill: parent
                        ToolButton {
                            text: qsTr("‹")
                            onClicked: stack.pop()
                        }
                        Label {
                            text: "Settings"
                            elide: Label.ElideRight
                            horizontalAlignment: Qt.AlignHCenter
                            verticalAlignment: Qt.AlignVCenter
                            Layout.fillWidth: true
                        }
                    }
                }
                Rectangle {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    color: "#d0d4e0"
                    ListView {
                        anchors.fill: parent
                        model: setListModel
                        delegate: ItemDelegate {
                            text: name
                            width: parent.width
                            onClicked: stackSettings.push(page)
                            Component.onCompleted: {
                                if(index === 0)
                                    if(winMain.wConfigured)
                                        text = winMain.wLocation + " " + winMain.wSublocation

                            }
                        }
                    }
                }
            }
        }
    }
}
