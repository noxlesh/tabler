#ifndef TODAYWEATHERPROVIDER_H
#define TODAYWEATHERPROVIDER_H
#include <QRegularExpression>
#include "httpdataprovider.h"



class TodayWeatherProvider : public HTTPDataProvider
{
    Q_OBJECT
    QQmlContext *ctx;
    enum WeatherStatus { Clear, LightCloud,
                        PartlyCloudy,Cloudy,MostlyCloudy,Undefined };
    WeatherStatus parseWeatherStatus(QString rawStr);

public:
    TodayWeatherProvider(QQmlContext *ctx);

signals:
    void weatherUpdated(QString temp);

public slots:
    void onConnFinished();
    void onConnError(QNetworkReply::NetworkError err);
};

#endif // TODAYWEATHERPROVIDER_H
