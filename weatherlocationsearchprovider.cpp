#include "weatherlocationsearchprovider.h"

WeatherLocationSearchProvider::WeatherLocationSearchProvider(QQmlContext *ctx)
{
    model = new WeatherLocationModel(this);
    //qDebug << this->objectName();
    ctx->setContextProperty("wlsp", this);
    ctx->setContextProperty("wlspModel", model);
}

void WeatherLocationSearchProvider::onConnFinished()
{
    model->clearData();
    while (!rply->atEnd()) {
        QString s = rply->readLine();
        qDebug() << s;
        model->addLocation(s.simplified());
    }
}

void WeatherLocationSearchProvider::onConnError(QNetworkReply::NetworkError err)
{
    qDebug() << err;
}
