#include <QGuiApplication>
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include "weatherlocationsearchprovider.h"
#include "todayweatherprovider.h"



int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QQmlContext *ctx = engine.rootContext();
    auto *wlsp = new WeatherLocationSearchProvider(ctx);
    auto *twp = new TodayWeatherProvider(ctx);
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
