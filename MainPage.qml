import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

Item {
    MouseArea {
        anchors.fill: parent
        onClicked: stack.push("qrc:/SettingsPage.qml")
    }
    ColumnLayout {
        anchors.fill: parent
        spacing: 0
        ClockWidget {
            id: clockWidget
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: 200
            Layout.preferredHeight: 280
            Layout.maximumHeight: 280
            Layout.alignment: Qt.AlignTop
        }
        WeatherWidget {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredHeight: 200
            Layout.minimumHeight: 200
            Layout.maximumHeight: 200
            Layout.alignment: Qt.AlignBottom

        }
    }

}
