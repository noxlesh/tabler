#include "weatherlocationmodel.h"

WeatherLocation::WeatherLocation(const QString &location, const QString &sublocation, const QString &urlParam) :
    m_location(location),
    m_sublocation(sublocation),
    m_urlParam(urlParam)
{

}

QString WeatherLocation::location() const
{
    return m_location;
}

QString WeatherLocation::sublocation() const
{
    return m_sublocation;
}

QString WeatherLocation::urlParam() const
{
    return m_urlParam;
}

//
// WeatherLocationModel
//

WeatherLocationModel::WeatherLocationModel(QObject *parent) : QAbstractListModel(parent)
{

}

void WeatherLocationModel::addLocation(const QString &locationStr)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    QStringList locationParams = locationStr.split('|');
    wLocations.append(WeatherLocation(locationParams[0],locationParams[1],locationParams[2]));
    endInsertRows();
}

void WeatherLocationModel::clearData()
{
    beginResetModel();
    wLocations.clear();
    endResetModel();
}

int WeatherLocationModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return wLocations.count();
}

QVariant WeatherLocationModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= wLocations.count())
            return QVariant();
    const WeatherLocation &location = wLocations[index.row()];
    if (role == LocationRole)
        return location.location();
    else if (role == SublocationRole)
        return location.sublocation();
    else if (role == UrlParamRole)
        return location.urlParam();
    return QVariant();
}

QHash<int, QByteArray> WeatherLocationModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[LocationRole] = "location";
    roles[SublocationRole] = "sublocation";
    roles[UrlParamRole] = "urlParam";
    return roles;
}




