#ifndef WEATHERLOCATIONMODEL_H
#define WEATHERLOCATIONMODEL_H
#include <QAbstractListModel>

class WeatherLocation
{
public:
    WeatherLocation(const QString &location, const QString &sublocation, const QString &urlParam);
    QString location() const;
    QString sublocation() const;
    QString urlParam() const;
private:
    QString m_location;
    QString m_sublocation;
    QString m_urlParam;
};

class WeatherLocationModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum WeatherLocationRoles {
        LocationRole = Qt::UserRole + 1,
        SublocationRole,
        UrlParamRole
    };
    WeatherLocationModel(QObject *parent = 0);
    void addLocation(const QString &locationStr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
public slots:
    void clearData();
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<WeatherLocation> wLocations;
};

#endif // WEATHERLOCATIONMODEL_H
