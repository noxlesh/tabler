#ifndef WEATHERLOCATIONSEARCHPROVIDER_H
#define WEATHERLOCATIONSEARCHPROVIDER_H
#include "httpdataprovider.h"
#include "weatherlocationmodel.h"


class WeatherLocationSearchProvider : public HTTPDataProvider
{
    Q_OBJECT
public:
    WeatherLocationSearchProvider(QQmlContext *ctx);

public slots:
    void onConnFinished();
    void onConnError(QNetworkReply::NetworkError err);
private:
    QNetworkAccessManager *qnam;
    WeatherLocationModel *model;
};

#endif // WEATHERLOCATIONSEARCHPROVIDER_H
